namespace Hestia.Model.Builders
{
    public interface IRepositorySnapshotBuilderWrapper
    {
        RepositorySnapshot Build(RepositorySnapshotBuilderArguments args);
    }
}
